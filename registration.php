<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Elatebrain_Framework',
    __DIR__
);
