<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Helper;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 *
 */
class AbstractHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     */
    public const CONFIG_MODULE_PATH = 'elatebrain_core';

    /**
     * @var Serialize
     */
    protected Serialize $serialize;
    /**
     * @var Json
     */
    protected Json $json;
    /**
     * @var RemoteAddress
     */
    protected RemoteAddress $remoteAddress;
    /**
     * @var Session
     */
    protected Session $customerSession;

    /**
     * @var ModuleListInterface
     */
    protected ModuleListInterface $moduleList;

    /**
     * @var array
     */
    protected array $elatebrainModules = [];

    /**
     * @param Context $context
     * @param Serialize $serialize
     * @param Json $json
     * @param RemoteAddress $remoteAddress
     * @param Session $customerSession
     * @param ModuleListInterface $moduleList
     */
    public function __construct(
        Context $context,
        Serialize $serialize,
        Json $json,
        RemoteAddress $remoteAddress,
        Session $customerSession,
        ModuleListInterface $moduleList
    ) {
        parent::__construct($context);
        $this->serialize = $serialize;
        $this->json = $json;
        $this->remoteAddress = $remoteAddress;
        $this->customerSession = $customerSession;
        $this->moduleList = $moduleList;
    }

    /**
     * @param $field
     * @param $scopeValue
     * @param string $scopeType
     * @return mixed
     */
    public function getConfigValue($field, $scopeValue = null, string $scopeType = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($field, $scopeType, $scopeValue);
    }

    /**
     * @param string $code
     * @param $storeId
     * @return mixed
     */
    public function getGeneralConfiguration(string $code = '', $storeId = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/general_configuration' . $code, $storeId);
    }

    /**
     * @param string $field
     * @param $storeId
     * @return mixed
     */
    public function getSpecificConfiguration(string $field = '', $storeId = null)
    {
        $field = ($field !== '') ? '/' . $field : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . $field, $storeId);
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function serialize($data)
    {
        return $this->serialize->serialize($data);
    }

    /**
     * @param $string
     * @return array|bool|float|int|mixed|string|null
     */
    public function unserialize($string)
    {
        return $this->serialize->unserialize($string);
    }

    /**
     * @param $valueToEncode
     * @return bool|string
     */
    public function jsonEncode($valueToEncode)
    {
        $encodeValue = '{}';
        if ($valueToEncode != null) {
            try {
                $encodeValue = $this->json->serialize($valueToEncode);
            } catch (Exception $e) {
                $this->_logger->error($e->getMessage());
            }
        }

        return $encodeValue;
    }

    /**
     * @param $encodedValue
     * @return array|bool|float|int|mixed|string|null
     */
    public function jsonDecode($encodedValue)
    {
        $decodeValue = [];
        if ($encodedValue != null) {
            try {
                $decodeValue = $this->json->unserialize($encodedValue);
            } catch (Exception $e) {
                $this->_logger->error($e->getMessage());
            }
        }

        return $decodeValue;
    }

    /**
     * @param bool $ipToLong
     * @return false|string
     */
    public function getRemoteAddress(bool $ipToLong = false)
    {
        return $this->remoteAddress->getRemoteAddress($ipToLong);
    }

    /**
     * @return false|Customer
     */
    public function getCurrentCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            return $this->customerSession->getCustomer();
        }

        return false;
    }

    /**
     * @param array $excludes
     * @return array
     */
    public function getElatebrainExtensions(array $excludes = []): array
    {
        if ($this->elatebrainModules == null) {
            $this->elatebrainModules = [];
            $modules = $this->moduleList->getAll();
            if (is_array($modules)) {
                foreach ($modules as $moduleName => $module) {
                    if (strpos($moduleName, 'Elatebrain_') !== false && !in_array($moduleName, $excludes)) {
                        $this->elatebrainModules[$moduleName] = $module;
                    }
                }
            }
        }

        return $this->elatebrainModules;
    }
}
