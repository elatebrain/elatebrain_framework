<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Observer;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Elatebrain\Framework\Model\Modules;
use Magento\Framework\HTTP\PhpEnvironment\ServerAddress;

/**
 *
 */
class ConfigObserver implements ObserverInterface
{
    /**
     * @var Curl
     */
    protected Curl $curl;
    /**
     * @var Modules
     */
    private Modules $modules;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var ServerAddress
     */
    private ServerAddress $serverAddress;
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @param Curl $curl
     * @param Modules $modules
     * @param StoreManagerInterface $storeManager
     * @param ServerAddress $serverAddress
     * @param LoggerInterface $logger
     */
    public function __construct(
        Curl $curl,
        Modules $modules,
        StoreManagerInterface $storeManager,
        ServerAddress $serverAddress,
        LoggerInterface $logger
    ) {
        $this->curl = $curl;
        $this->modules = $modules;
        $this->storeManager = $storeManager;
        $this->serverAddress = $serverAddress;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $configData = $observer->getEvent()->getData('configData');

            if (
                is_array($configData) &&
                isset($configData['section']) && strpos($configData['section'], 'elatebrain_') !== false
            ) {
                $modules = $this->modules->getModuleList();
                $activationKey = "";

                if (isset($configData['groups']['extension']['fields']['activation_key']['value'])) {
                    $activationKey = $configData['groups']['extension']['fields']['activation_key']['value'];
                }

                foreach ($modules as $moduleName => $moduleInfo) {
                    $url = $this->storeManager->getStore()->getBaseUrl();
                    $domain = parse_url($url, PHP_URL_HOST);
                    $domains = parse_url($url, PHP_URL_HOST);
                    $data = [];
                    $data['extension_name'] = $moduleName;
                    $data['extension_version'] = (isset($moduleInfo['version'])) ? $moduleInfo['version'] : '';
                    $data['site_url'] = $url;
                    $data['site_domain'] = $domain;
                    $data['store_domains'] = $domains;
                    $data['server_ip'] = $this->serverAddress->getServerAddress();
                    $data['license_key'] = $activationKey;

                    $apiUrl = 'https://store.elatebrain.com/customer/tracker/save';

                    $this->curl->post($apiUrl, $data);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
