<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Block\Adminhtml\System;

use Magento\Backend\Block\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Config\Block\System\Config\Form\Fieldset;
use Magento\Framework\App\State;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\View\Helper\Js;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

/**
 *
 */
class Information extends Fieldset
{
    /**
     * @var DirectoryList
     */
    protected DirectoryList $directoryList;
    /**
     * @var State
     */
    protected State $appState;

    /**
     * @param Context $context
     * @param Session $authSession
     * @param Js $jsHelper
     * @param DirectoryList $directoryList
     * @param State $appState
     * @param SecureHtmlRenderer|null $secureRenderer
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $authSession,
        Js $jsHelper,
        DirectoryList $directoryList,
        State $appState,
        ?SecureHtmlRenderer $secureRenderer = null,
        array $data = []
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data, $secureRenderer);
        $this->directoryList = $directoryList;
        $this->appState = $appState;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $content = $this->_getHeaderHtml($element);
        $content .= $this->getSystemInformation($element);
        $content .= $this->_getFooterHtml($element);

        return $content;
    }

    /**
     * @param $fieldset
     * @return string
     */
    private function getSystemInformation($fieldset): string
    {
        $content = "";
        $content .= $this->getMagentoRootPath($fieldset);
        $content .= $this->getMagentoDeployMode($fieldset);
        $content .= $this->getPHPVersion($fieldset);

        return $content;
    }

    /**
     * @param $fieldset
     * @return mixed
     */
    private function getMagentoRootPath($fieldset)
    {
        $label = __("Magento Root Path");
        $path = $this->directoryList->getRoot();
        return $this->getFieldOutput($fieldset, "magento_root_path", $label, $path);
    }

    /**
     * @param $fieldset
     * @return mixed
     */
    private function getMagentoDeployMode($fieldset)
    {
        $label = __("Magento Current Deploy Mode");
        $mode = ucwords($this->appState->getMode());
        return $this->getFieldOutput($fieldset, "magento_deploy_mode", $label, $mode);
    }

    /**
     * @param $fieldset
     * @return mixed
     */
    private function getPHPVersion($fieldset)
    {
        $label = __("PHP Version");
        $version = phpversion();
        return $this->getFieldOutput($fieldset, "php_version", $label, $version);
    }

    /**
     * @param $fieldset
     * @param $fieldName
     * @param string $label
     * @param string $value
     * @return mixed
     */
    protected function getFieldOutput($fieldset, $fieldName, string $label = '', string $value = '')
    {
        $name = strtolower(str_replace(" ", "", $label));
        $field = $fieldset->addField($fieldName, 'label', [
            'name'  => $name,
            'label' => $label,
            'value' => $value,
            'bold' => true
        ]);

        return $field->toHtml();
    }
}
