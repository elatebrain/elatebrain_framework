<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Block\Adminhtml\System\Config\Form\Field\FieldArray;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\DataObject;

/**
 *
 */
class TimeArray extends AbstractFieldArray
{
    /**
     * @var Factory
     */
    protected Factory $elementFactory;

    /**
     * @var string
     */
    protected string $template = 'Elatebrain_Framework::system/config/form/field/time-array.phtml';

    /**
     * @param Context $context
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->elementFactory = $elementFactory;
    }

    /**
     * @return void
     */
    protected function _prepareToRender(): void
    {
        $this->addColumn('from', ['label' => __('From'), 'class' => 'required-entry']);
        $this->addColumn('to', ['label' => __('To'), 'class' => 'required-entry']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @param $columnName
     * @return string
     * @throws Exception
     */
    public function renderCellTemplate($columnName): string
    {
        if (!empty($this->_columns[$columnName]) && in_array($columnName, ['from', 'to'])) {
            $element = $this->elementFactory->create('time');
            $element->setForm($this->getForm())
                ->setName($this->_getCellInputElementName($columnName))
                ->setHtmlId($this->_getCellInputElementId('<%- _id %>', $columnName));

            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }

    /**
     * @param DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $html = parent::_getElementHtml($element);

        $html .= "<style>
        #sldelivery_information_time_configuration_time_slots tbody tr td span.time-separator:nth-of-type(2){
        display:none}
        #sldelivery_information_time_configuration_time_slots tbody tr td select:nth-of-type(3){display:none}
        </style>";
        return $html;
    }
}
