<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\PackageInfoFactory;

/**
 *
 */
class Version extends Field
{
    /**
     * @var PackageInfoFactory
     */
    protected PackageInfoFactory $packageInfoFactory;

    /**
     * @param Context $context
     * @param PackageInfoFactory $packageInfoFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PackageInfoFactory $packageInfoFactory,
        array $data = []
    ) {
        $this->packageInfoFactory = $packageInfoFactory;

        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $elementData = $element->getOriginalData();

        $packageInfo = $this->packageInfoFactory->create();
        $version = $packageInfo->getVersion($elementData['extension_name']);

        return '<div class="extension-version-individual">' . $version . '</div>';
    }

    /**
     * @param AbstractElement $element
     * @param $html
     * @return string
     */
    protected function _decorateRowHtml(AbstractElement $element, $html): string
    {
        return '<tr id="row_' . $element->getHtmlId() . '" class="individual_extension_version">' . $html . '</tr>';
    }
}
