<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Block\Adminhtml\System;

use Exception;
use Magento\Backend\Block\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Config\Block\System\Config\Form\Fieldset;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\Js;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Elatebrain\Framework\Model\Modules;

/**
 *
 */
class Extensions extends Fieldset
{
    /**
     * @var Modules
     */
    private Modules $modules;

    /**
     * @param Context $context
     * @param Session $authSession
     * @param Js $jsHelper
     * @param Modules $modules
     * @param SecureHtmlRenderer|null $secureRenderer
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $authSession,
        Js $jsHelper,
        Modules $modules,
        ?SecureHtmlRenderer $secureRenderer = null,
        array $data = []
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data, $secureRenderer);
        $this->modules = $modules;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $content = $this->_getHeaderHtml($element);
        $content .= $this->getExtensionList();
        $content .= $this->_getFooterHtml($element);

        return $content;
    }

    /**
     * @return string
     */
    private function getExtensionList(): string
    {
        $content = "";
        try {
            $modules = $this->modules->getModuleList();

            $elatebrainModules = "";
            foreach ($modules as $name => $moduleInfo) {
                $elatebrainModules .= "<li>";
                if (isset($moduleInfo['product_url'])) {
                    $elatebrainModules .= '<a href="' . $moduleInfo['product_url'] . '" target="_blank">';
                    $elatebrainModules .= "<span>" . ((isset($moduleInfo['name'])) ? $moduleInfo['name'] : $name) . "
                        </span>";
                    $elatebrainModules .= '</a>';
                } else {
                    $elatebrainModules .= "<span>" . ((isset($moduleInfo['name'])) ? $moduleInfo['name'] : $name) . "
                        </span>";
                }
                if (isset($moduleInfo['extension_version']) && isset($moduleInfo['version'])) {
                    if ($moduleInfo['extension_version'] == $moduleInfo['version']) {
                        $elatebrainModules .= "<span class='up-to-date'>" . $moduleInfo['version'] . "</span>";
                    } else {
                        $elatebrainModules .= "<span class='out-date'>
                                Installed Version: " . $moduleInfo['version'] . "<br/>
                                Latest Version: " . $moduleInfo['extension_version'] . "</span>";
                    }
                } elseif (isset($moduleInfo['version'])) {
                    $elatebrainModules .= "<span class='installed-version'>" . $moduleInfo['version'] . "</span>";
                }

                $elatebrainModules .= "</li>";
            }
            if ($elatebrainModules) {
                $content = '<ul>' . $elatebrainModules . '</ul>';
            }
        } catch (Exception $e) {
            $this->_logger->error($e->getMessage());
        }

        return $content;
    }

    /**
     * @param $fieldset
     * @param $fieldName
     * @param string $label
     * @param string $value
     * @return mixed
     */
    protected function getFieldOutput($fieldset, $fieldName, string $label = '', string $value = '')
    {
        $name = strtolower(str_replace(" ", "", $label));
        $field = $fieldset->addField($fieldName, 'label', [
            'name'  => $name,
            'label' => $label,
            'value' => $value,
            'bold' => true
        ]);

        return $field->toHtml();
    }
}
