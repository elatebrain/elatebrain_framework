<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Model;

use Elatebrain\Framework\Helper\AbstractHelper;
use Exception;
use Magento\Framework\Component\ComponentRegistrarInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Module\PackageInfoFactory;

/**
 *
 */
class Modules
{
    /**
     * @var ComponentRegistrarInterface
     */
    protected ComponentRegistrarInterface $moduleRegistry;
    /**
     * @var File
     */
    protected File $filesystemDriver;
    /**
     * @var Json
     */
    protected Json $json;

    /**
     * @var Curl
     */
    protected Curl $curl;

    /**
     * @var array
     */
    protected array $elatebrainModules = [];

    /**
     * @var AbstractHelper
     */
    protected AbstractHelper $helper;

    /**
     * @var PackageInfoFactory
     */
    protected PackageInfoFactory $packageInfoFactory;

    /**
     * @param ComponentRegistrarInterface $moduleRegistry
     * @param File $filesystemDriver
     * @param Json $json
     * @param Curl $curl
     * @param AbstractHelper $helper
     * @param PackageInfoFactory $packageInfoFactory
     */
    public function __construct(
        ComponentRegistrarInterface $moduleRegistry,
        File $filesystemDriver,
        Json $json,
        Curl $curl,
        AbstractHelper $helper,
        PackageInfoFactory $packageInfoFactory
    ) {
        $this->moduleRegistry = $moduleRegistry;
        $this->filesystemDriver = $filesystemDriver;
        $this->json = $json;
        $this->curl = $curl;
        $this->helper = $helper;
        $this->packageInfoFactory = $packageInfoFactory;
    }

    /**
     * @return array
     */
    public function getModuleList(): array
    {
        if ($this->elatebrainModules == null) {
            $this->elatebrainModules = [];
            $modules = $this->helper->getElatebrainExtensions(["Elatebrain_Framework"]);
            $feed = $this->getFeed();
            foreach ($modules as $moduleName => $moduleDetails) {
                $extensionInfo = [];
                $packageInfo = $this->packageInfoFactory->create();
                $extensionInfo['version'] = $packageInfo->getVersion($moduleName);
                $extensionInfo['name'] = $moduleName;
                if ($extensionName = $packageInfo->getPackageName($moduleName)) {
                    $extensionInfo['name'] = $extensionName;
                }
                if (isset($extensionInfo['name']) && $extensionInfo['name'] != null) {
                    foreach ($feed as $extension) {
                        if (
                            isset($extension['extension_composer_name']) &&
                            trim($extension['extension_composer_name']) == trim($extensionInfo['name'])
                        ) {
                            $extensionInfo = array_merge($extensionInfo, $extension);
                        }
                    }
                }

                $this->elatebrainModules[$moduleName] = $extensionInfo;
            }
        }

        return $this->elatebrainModules;
    }

    /**
     * @return array
     */
    private function getFeed(): array
    {
        $response = [];
        try {
            $this->curl->get("https://store.elatebrain.com/rest/all/V1/extensions");
            if ($this->curl->getStatus() == 200) {
                $response = $this->curl->getBody();
                if ($response = $this->json->unserialize($response)) {
                    return $response;
                }
            }
        } catch (Exception $e) {
        }
        return $response;
    }
}
