<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\Model\Config\Structure;

use Elatebrain\Framework\Helper\AbstractHelper;
use Magento\Config\Model\Config\Structure\Data as StructureData;
use Elatebrain\Framework\Block\Adminhtml\System\Config\Form\Field\Version;

/**
 *
 */
class Data
{
    /**
     * @var AbstractHelper
     */
    protected AbstractHelper $helper;

    /**
     * @param AbstractHelper $helper
     */
    public function __construct(AbstractHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param StructureData $object
     * @param array $config
     * @return array[]
     */
    public function beforeMerge(StructureData $object, array $config): array
    {
        if (!isset($config['config']['system']['sections'])) {
            return [$config];
        }

        $sections = $config['config']['system']['sections'];
        if (is_array($sections)) {
            foreach ($sections as $sectionId => $section) {
                if (
                    isset($section['tab']) && ($section['tab'] == 'elatebrain') &&
                    ($section['id'] !== 'elatebrain_core')
                ) {
                    foreach (
                        $this->helper->getElatebrainExtensions(["Elatebrain_Framework"]) as $moduleName => $module
                    ) {
                        $dynamicGroups = $this->getDynamicConfigurationFieldsGroup($moduleName, $section['id']);
                        if (!empty($dynamicGroups)) {
                            $children = $section['children'];
                            $children = array_merge($dynamicGroups, $children);
                            $config['config']['system']['sections'][$sectionId]['children'] = $children;
                        }
                        break;
                    }
                }
            }
        }

        return [$config];
    }

    /**
     * @param $moduleName
     * @param $sectionName
     * @return array[]
     */
    protected function getDynamicConfigurationFieldsGroup($moduleName, $sectionName): array
    {
        $defaultFieldOptions = [
            'type'              => 'text',
            'showInDefault'     => '1',
            'showInWebsite'     => '0',
            'showInStore'       => '0',
            'sortOrder'         => 1,
            'extension_name'    => $moduleName,
            '_elementType'      => 'field',
            'path'              => $sectionName . '/extension'
        ];

        $fields = [];
        foreach ($this->getFieldList() as $id => $option) {
            if (isset($option['show'])) {
                continue;
            }

            $fields[$id] = array_merge($defaultFieldOptions, ['id' => $id], $option);
        }

        return [
            'extension' => [
                'id'            => 'extension',
                'label'         => __('Extension Activation'),
                'showInDefault' => '1',
                'showInWebsite' => '0',
                'showInStore'   => '0',
                '_elementType'  => 'group',
                'path'          => $sectionName,
                'children'      => $fields
            ]
        ];
    }

    /**
     * @return array[]
     */
    protected function getFieldList(): array
    {
        return [
            'version'     => [
                'type'           => 'label',
                'label'          => __('Installed Version'),
                'frontend_model' => Version::class,
            ],
            'name' => [
                'label'          => __('Register Name'),
                'frontend_class' => 'elatebrain-extension-activation-name'
            ],
            'email' => [
                'label'          => __('Register Email'),
                'validate'       => 'validate-email',
                'frontend_class' => 'elatebrain-extension-activation-email',
                'comment'        => __('This email will be used at Elatebrain account and support desk.')
            ],
            'activation_key' => [
                'label'          => __('Activation Key'),
                'frontend_class' => 'elatebrain-extension-activation-key',
                'validate'       => 'required-entry'
            ]
        ];
    }
}
