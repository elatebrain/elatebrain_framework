# Elatebrain Framework

## How to install or upgrade Elatebrain Framework

### 1. Install using composer (recommended)

We recommend you to install Elatebrain_Framework extension using composer. It is convenient to install, update and maintain.

Run below SSH command from Magento Root directory.

#### 1.1 Install

```
composer require elatebrain/module-framework
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
```

#### 1.2 Upgrade

```
composer update elatebrain/module-framework
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
```

### 2. Copy and paste

If you are not able to install using composer, you can install it through this way.

- Download [the latest version here](https://bitbucket.org/elatebrain/elatebrain_framework/get/master.zip)
- Extract `master.zip` file to `app/code/Elatebrain/Framework`. You should create a folder path `app/code/Elatebrain/Framework` if not exist.
- Go to Magento root folder and run below SSH commands to install `Elatebrain_Framework`:

```
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```
