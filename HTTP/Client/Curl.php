<?php

/*
 * Copyright © 2022 Elatebrain Private Limited. All rights reserved.
 * See LICENSE.txt for license details.
 *
 */

namespace Elatebrain\Framework\HTTP\Client;

use Magento\Framework\HTTP\Client\Curl as CoreCurl;

/**
 *
 */
class Curl extends CoreCurl
{
    /**
     * @param $uri
     * @param array $params
     * @return void
     */
    public function delete($uri, array $params = [])
    {
        $this->makeRequest("DELETE", $uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @return void
     */
    public function put($uri, array $params = [])
    {
        $this->makeRequest("PUT", $uri, $params);
    }
}
